from src.listas import ListaLigada


def main():
    # Crear una instancia de ListaLigada
    mi_lista = ListaLigada()

    # Agregar elementos a la lista
    mi_lista.agregar_elemento(1)
    mi_lista.agregar_elemento(2)
    mi_lista.agregar_elemento(3)
    mi_lista.agregar_elemento(4)

    # Imprimir la lista original
    print("Lista original:", mi_lista)

    # Eliminar la cabeza
    mi_lista.elimina_cabeza()
    print("Después de eliminar la cabeza:", mi_lista)

    # Eliminar el rabo
    mi_lista.elimina_rabo()
    print("Después de eliminar el rabo:", mi_lista)

    # Agregar más elementos
    mi_lista.agregar_elemento(5)
    mi_lista.agregar_elemento(6)

    # Imprimir la lista final
    print("Lista final:", mi_lista)


if __name__ == "__main__":
    main()
