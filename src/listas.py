from src.nodos import Nodo

# Definición de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        # Crear un nuevo nodo con el valor
        nuevo_nodo = Nodo(valor)

        # Si la lista está vacía, el nuevo nodo es la cabeza
        if self.cabeza is None:
            self.cabeza = nuevo_nodo
        else:
            # Si la lista no está vacía, encuentra el último nodo
            ultimo_nodo = self.cabeza
            while ultimo_nodo.siguiente is not None:
                ultimo_nodo = ultimo_nodo.siguiente

            # Ahora, el último nodo apunta al nuevo nodo
            ultimo_nodo.siguiente = nuevo_nodo

    # Busca un elemento dentro de la lista
    def buscar_elemento(self, valor):
        actual = self.cabeza
        posicion = 0

        while actual != None:
            # Compara el valor con el valor buscado
            if actual.valor == valor:
                return True
            else :
                actual = actual.siguiente  # Pasar al siguiente nodo
            posicion += 1
        return False  # Si no se encuentra el valor, devuelve False

    # Busca el elemento del principio y lo elimina
    def elimina_cabeza(self):
        if self.cabeza is not None:
            nodo_a_eliminar = self.cabeza
            self.cabeza = nodo_a_eliminar.siguiente
            nodo_a_eliminar.siguiente = None
            return nodo_a_eliminar.valor  # Retorna el valor del nodo eliminado
        # Si llegamos aquí, la lista está vacía
        return None  # La lista está vacía, no hay nada que eliminar

    # Elimina el último elemento de la lista
    def elimina_rabo(self):
        if self.cabeza is None:
            return  # La lista está vacía, no hay nada que eliminar

        if self.cabeza.siguiente is None:
            self.cabeza = None  # Si solo hay un nodo, lo eliminamos
            return

        actual = self.cabeza
        while actual.siguiente.siguiente is not None:
            actual = actual.siguiente

        actual.siguiente = None

    # Proporciona el tamaño de la lista
    def tamagno(self):
        contador = 0
        actual = self.cabeza  # Comenzamos desde la cabeza de la lista

        while actual is not None:
            contador += 1
            actual = actual.siguiente  # Avanzamos al siguiente nodo
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
